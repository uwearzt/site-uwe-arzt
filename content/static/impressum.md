+++
title = "Impressum"
path = "impressum"
+++

Uwe Arzt<br/>

Danziger Str. 104<br/>
41352 Korschenbroich<br/>
Germany

Phone: +49 171 5477670

E-Mail: <mailto:mail@uwe-arzt.de><br/>
WWW: <https://uwe-arzt.de>

For contacting via EMail, please us my gpg [key](@/static/gpgkey.md).

### german legal stuff

Inhaltlich Verantwortlicher gem. § 55 II RStV: Uwe Arzt (Anschrift s.o.)

### License/Copyright

[GNU General Public License v3.0 or later](https://spdx.org/licenses/GPL-3.0-or-later.html)
