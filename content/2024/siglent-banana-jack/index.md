+++
title = "Siglent SDL1020X-E Banana Jack Adapter"
date = 2024-04-28

aliases = [
  "4268c63a"
]

[taxonomies]
categories = ["electronic"]
+++

I bought a Siglent SDL1030X power supply, and i wanted to use banana jacks
to connect to my systems under test. There are already some adapters available,
but i wanted to design my own, because i wanted to have a XT60 Adapter as well.

The Video from [Clough42](https://www.youtube.com/watch?v=2M9VKDTun3c) has a
nice step by step approach to design the adapter, i followed.

You can find thecomplete project in my
[kicad-parts repository](https://codeberg.org/uwearzt/kicad-parts/src/branch/main/siglent_adapter).

Here the mounted Adapter:

![adapter mounted on Siglent](mounted.jpeg)

<!-- more -->

## step by step the design process

### Fitting test with 3D print

![fitting test with 3D print](test_3dprint.jpeg)

### Adapter parts

![adapter parts](adapter_parts.jpeg)

### Assembled adapter

![assembled adapter](adapter_assembled.jpeg)

### Closeup adapter mounted

![closeup adapter mounted on Siglent](mounted_closeup.jpeg)
