+++
title = "Rhino 3D tips"

aliases = [
  "424eb73f"
]
+++

## Keys

| Icon     | Key |
| -------- | --- |
| &#x2318; | command |
| &#x2303; | control |
| &#x2325; | alt |
| &#x21e7; | shift |
| &#x232b; | delete |

## Keyboard Shortcuts

| Shortcut            | Command |
| ------------------- | --- |
| &#x21e7; &#x2318; E | Zoom extents |
| &#x2325; &#x2318; E | Zoom extents all |
| &#x2303; &#x2318; F | Full screen |
| &#x2318; F1         | Maximize Viewport Top |
| &#x2318; F2         | Maximize Viewport Front |
| &#x2318; F3         | Maximize Viewport Right |
| &#x2318; &#x232b;   | Delete
| &#x2318; A          | Select all |
| &#x2318; G          | Group |
| &#x21e7; &#x2318; G | Ungroup |
| &#x2318; J          | Join |
| &#x2303; &#x21e7; S | Split |
| &#x2318; T          | Trim |
| &#x2318; Z          | Undo |
| &#x21e7; &#x2318; Z | Redo |
| F3                  | Object properties |
| F9                  | Toggle grid snap |

## Commands

| Command             | Description |
| ------------------- | --- |
| ToolbarReset        | Reset the Toolbar Settings to standard |
