# Uwes Website and Blog

[![GPLv3 licensed](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://spdx.org/licenses/GPL-3.0-or-later.html)

This is the source for [Uwes Blog](https://uwe-arzt.de). I use
[Zola](https://www.getzola.org) to
take the source here and create a static site.

## URL scheme

* Blog:  ```/[year]/[name]```
* Reference: ```/reference/[name]```
* Static: ```/[name]```

## Categories

* programming
* robotic
* cad
* electronic
* misc

## License

GNU General Public License, Version 3.0 [LICENSE](LICENSE) or
[https://spdx.org/licenses/GPL-3.0-or-later.html](https://spdx.org/licenses/GPL-3.0-or-later.html)
