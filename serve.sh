#!/bin/bash
# ------------------------------------------------------------------------------
# Copyright 2018-2023 Uwe Arzt, mail@uwe-arzt.de
# SPDX-License-Identifier: GPL-3.0-or-later
# ------------------------------------------------------------------------------
# set -e
# set -x

. zola.sh
echo "---------------------------------------------------------------------"
echo $ZOLA
$ZOLA --version
echo "---------------------------------------------------------------------"

$ZOLA build
$ZOLA serve
