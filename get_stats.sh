#!/bin/bash
# ------------------------------------------------------------------------------
# Copyright 2018-2023 Uwe Arzt, mail@uwe-arzt.de
# SPDX-License-Identifier: GPL-3.0-or-later
# ------------------------------------------------------------------------------
# set -e
# set -x

# ------------------------------------------------------------------------------
SERVER=hetzner01
LOGDIR=/var/log/caddy/
LOGFILE=site-uwe-arzt.log

LOCALSTATSDIR=/tmp/
REPORT=site-uwe-arzt.html

# ------------------------------------------------------------------------------
echo "get logfile"
scp $SERVER:$LOGDIR$LOGFILE $LOCALSTATSDIR

echo "generate report"
goaccess $LOCALSTATSDIR$LOGFILE --log-format=CADDY --output=$LOCALSTATSDIR$REPORT

