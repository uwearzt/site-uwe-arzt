#!/bin/bash
# ------------------------------------------------------------------------------
# Copyright 2021-2023 Uwe Arzt, mail@uwe-arzt.de
# SPDX-License-Identifier: GPL-3.0-or-later
# ------------------------------------------------------------------------------
# set -e
# set -x

ZOLA=/opt/homebrew/bin/zola
# ZOLA=/Users/uwe/repo/web/zola/target/debug/zola
